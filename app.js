const express = require("express");
const bodyParser = require("body-parser");
const { Sequelize } = require('sequelize');
//routes 
//app declaration
const app = express();
var cron = require('node-cron');
const userRoutes = require("./routes/user");
const blogRoutes = require("./routes/blog");
const forumRoutes = require("./routes/forum");
const forumcommentRoutes = require("./routes/forumcomment");
const fileUploadRoutes = require("./routes/awsfileupload");
const contactUsRoutes = require("./routes/contactus");

var cors = require('cors');


  app.use(cors());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use("/api/user", userRoutes);
  app.use("/api/blog", blogRoutes);
  app.use("/api/forum", forumRoutes);
  app.use("/api/forumcomment", forumcommentRoutes);
  app.use("/api/aws", fileUploadRoutes);
  app.use("/api/contactus", contactUsRoutes);

module.exports = app;

cron.schedule('0 0 */12 * * *', function(){

  console.log("Cron Running")
  helper.expired_docs();
  // helper.invoice_reminder();
  // helper.partial_customers();

});

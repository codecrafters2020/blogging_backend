'use strict';
const Sequelize = require("sequelize");
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserPrivilege extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  UserPrivilege.init({
    description: DataTypes.STRING,
    allowed_type:{
      type: Sequelize.ENUM,
      values: ['admin', 'customer'] 
      
    } ,
   // amount: Sequelize.INTEGER
  }, {
    sequelize,
    modelName: 'UserPrivilege',
    tableName: 'user_privileges'
  });
  return UserPrivilege;
};
'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class contactus extends Model {
 
    static associate(models) {
      // define association here
    }
  };
  contactus.init({
    name: DataTypes.STRING,
    company_name: DataTypes.STRING,
    message: DataTypes.STRING,
    reason: DataTypes.STRING,
    email: DataTypes.STRING,
    contact_number: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'contactus',
  });
  return contactus;
};
'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class blogs extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      blogs.belongsTo(models.User, {foreignKey: 'createdby'})

    }
  };
  blogs.init({
    createdby: DataTypes.INTEGER,
    title: DataTypes.STRING,
    text: DataTypes.STRING,
    image: DataTypes.STRING,
    tag: DataTypes.STRING,
    verified: DataTypes.BOOLEAN,
    read_count: DataTypes.INTEGER,
    featured: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'blogs',
  });
  return blogs;
};
'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class forum_comments extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    
    static associate(models) {
      forum_comments.belongsTo(models.User, {foreignKey: 'createdby'})

      // define association here
    }
  };
  forum_comments.init({
    createdby: DataTypes.INTEGER,
    comment: DataTypes.STRING,
    forumId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'forum_comments',
  });
  return forum_comments;
};
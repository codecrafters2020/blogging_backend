'use strict';
var Sequelize = require("sequelize");
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    
    static associate(models) {
     User.belongsTo(models.UserPrivilege, {foreignKey: 'userPrivilegeId'})
     User.hasMany(models.forum, {foreignKey:'createdby'})
     User.hasMany(models.forum_comments, {foreignKey:'createdby'})
     User.hasMany(models.blogs, {foreignKey:'createdby'})

     
      // User.belongsTo(Company);
      // define association here
    }
  };
  User.init({
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    email: DataTypes.STRING,
    encryptedPassword: DataTypes.STRING,
    lastSignIn: DataTypes.STRING,
    IP: DataTypes.STRING,
    signInCount: DataTypes.INTEGER,
    mobile: DataTypes.STRING,
    role: {
      type: Sequelize.ENUM,
      values: ['admin', 'customer']
    },
    userPrivilegeId: DataTypes.INTEGER,
    verifed: DataTypes.BOOLEAN,
    blackList: DataTypes.BOOLEAN,
    otp: DataTypes.BOOLEAN,
    
  }, {
    sequelize,
    modelName: 'User',
    tableName: 'users'
  });
  return User;
};
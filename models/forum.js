'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class forum extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    
    static associate(models) {
      // define association here

      forum.hasMany(models.forum_comments, {foreignKey:'forumId'})
      forum.belongsTo(models.User, {foreignKey: 'createdby'})
    }
  };
  forum.init({
    createdby: DataTypes.INTEGER,
    title: DataTypes.STRING,
    text: DataTypes.STRING,
    image: DataTypes.STRING,
    tag: DataTypes.STRING,
    verified: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'forum',
  });
  return forum;
};
const express = require("express");
const router = express.Router();

const forumController = require("../controllers/forums");
const checkAuth = require("../middlewares/check-auth");

router.get("/getAllforums", forumController.getAllforums);
router.get("/getforumsbyid", forumController.getforumsbyid);
router.get("/gettags", forumController.gettags);
router.get("/getforumbycategory", forumController.getforumbycategory);
router.get("/getAllMyforums", forumController.getAllMyforums);

router.post("/createforums", forumController.createforums);
router.delete("/deleteforums", forumController.deleteforums);
router.post("/verifyforum", forumController.verifyforum);

router.post("/unverifyforum", forumController.unverifyforum);
router.post("/markfeaturedforum", forumController.markfeaturedforum);
router.post("/markunfeaturedforum", forumController.markunfeaturedforum);


router.get("/getadminforums", forumController.getadminforums);

module.exports = router;

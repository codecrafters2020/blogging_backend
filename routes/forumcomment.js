const express = require("express");
const router = express.Router();

const forumcommentsController = require("../controllers/forum_comments");
const checkAuth = require("../middlewares/check-auth");


router.post("/createforumcomments", forumcommentsController.createforumcomments);
router.delete("/deleteforumcomments", forumcommentsController.deleteforumcomments);

module.exports = router;

const express = require("express");
const router = express.Router();

const blogController = require("../controllers/blogs");
const checkAuth = require("../middlewares/check-auth");

router.get("/getAllblogs", blogController.getAllblogs);
router.get("/getblogbyid", blogController.getblogbyid);
router.get("/gettags", blogController.gettags);
router.get("/getfeaturedblogs", blogController.getfeaturedblogs);
router.get("/getmostreadblogs", blogController.getmostreadblogs);
router.get("/getblogbycategory", blogController.getblogbycategory);
router.get("/getmyallblog", blogController.getAllMyblogs);


router.post("/createblog", blogController.createblog);
router.delete("/deleteblog", blogController.deleteblog);
router.post("/updateblog", blogController.updateblog);
router.post("/verifyblog", blogController.verifyblog);
router.post("/unverifyblog", blogController.unverifyblog);
router.post("/markfeaturedblog", blogController.markfeaturedblog);
router.post("/markunfeaturedblog", blogController.markunfeaturedblog);

router.get("/getAdminblogs", blogController.getAdminblogs);

module.exports = router;

const express = require("express");
const router = express.Router();

const UserController = require("../controllers/user");
const checkAuth = require("../middlewares/check-auth");

router.post("/signup", UserController.createUser);
router.post("/login", UserController.userLogin);

router.post("/resetpassword", UserController.resetPassword);
router.get("/forget_password", UserController.forgetPasswordEmail);

router.get("/verifyuser", UserController.verifyuser);
router.get("/allusers", UserController.getAllUsers);

router.get("/:id", UserController.getUserById);
router.put("/:id", UserController.updateUserById);


module.exports = router;

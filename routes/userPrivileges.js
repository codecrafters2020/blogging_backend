const express = require("express");
const router = express.Router();

const UserPriviligesController = require("../controllers/userPrivileges");
const checkAuth = require("../middlewares/check-auth");

router.get("/get_all_privileges", UserPriviligesController.getAllPrivileges);
router.get("/basic", UserPriviligesController.getBasicCustomerPrivilege);
router.get("/get_admin_type_privileges", UserPriviligesController.getAdminTypePrivileges);
// router.post("/checkOTP/:id", UserController.checkOTP);
// router.get("/get_all_admins",checkAuth,  UserController.getAdmins);
// router.get("/get_all_customers",checkAuth, UserController.getCustomers);
// router.get("/:id",checkAuth, UserController.getUserById);
// router.get("/getOTP/:id", UserController.getOTP);
// router.delete("/:id",checkAuth, UserController.deleteUserById);
// router.put("",checkAuth, UserController.updateUser);

module.exports = router;

dataBase = require('../models');
const blogs = dataBase.blogs;
const sequelize = require("sequelize");
//1 for newspress
//2 for memberpress

exports.getAllblogs = async (req, res, next) => {
    const paginate = (query, { page, pageSize }) => {
        const offset = page * pageSize;
        const limit = pageSize;
      
        return {
          ...query,
          offset,
          limit,
        };
      };

      blogs.findAll(
        paginate(
            {
                where:{ verified: true },
            },
            {
                page: req.query.page_no,// your page number
                pageSize:10,// your limit        
            }
   //      order: [["createdAt" ,'DESC']],
    ))
    .then(resData=>{                
        return res.status(200).json({
            blogs:resData
        })
    })
    .catch(err=>{
        console.log(err);
        return res.status(500).json({
            err:err
        });
    })
}

exports.getAllMyblogs = async (req, res, next) => {

    blogs.findAll({
        where:{ createdby:req.query.id },

        order: [["updatedAt" ,'DESC']],
        include: [
            {
              association:'User',
              attributes: ['id','email','firstName','lastName']                        
            }
          ],
    })
    .then(resData=>{                
        return res.status(200).json({
            blogs:resData
        })
    })
    .catch(err=>{
        console.log(err);
        return res.status(500).json({
            err:err
        });
    })
}

exports.getblogbyid = async (req, res, next) => {

    blogs.findOne({
        where:{ id: req.query.id  },
        include: [
            {
              association:'User',
              attributes: ['id','email','firstName','lastName']                        
            }
          ],
    })
    .then(resData=>{  
        
        resData.read_count =   resData.read_count +1 ;
        resData.save()

        return res.status(200).json({
            resData
        })
    })
    .catch(err=>{
        console.log(err);
        return res.status(500).json({
            err:err
        });
    })
}



exports.gettags = async (req, res, next) => {

    blogs.findAndCountAll({
        where:{ verified: true},

        attributes: [           
            // [sequelize.fn('DISTINCT', sequelize.col('tag')) ,'tag'],
            [sequelize.fn('COUNT', sequelize.col('blogs.id')), 'tags']
        ],                group: ['blogs.tag'],

    })
    .then(resData=>{                
        return res.status(200).json({
            tag:resData
        })
    })
    .catch(err=>{
        console.log(err);
        return res.status(500).json({
            err:err
        });
    })
}

exports.getfeaturedblogs= async (req, res, next) => {

    blogs.findAll({
        where:{ featured: true, verified: true}

    })
    .then(resData=>{                
        return res.status(200).json({
            tag:resData
        })
    })
    .catch(err=>{
        console.log(err);
        return res.status(500).json({
            err:err
        });
    })
}

exports.getmostreadblogs= async (req, res, next) => {

    blogs.findAll({
        where:{ verified: true},
        order: [["read_count" ,'DESC']],
        // order: [
           
        //     sequelize.fn( sequelize.col('read_count')),
          
        //   ],
    })
    .then(resData=>{                
        return res.status(200).json({
            tag:resData
        })
    })
    .catch(err=>{
        console.log(err);
        return res.status(500).json({
            err:err
        });
    })
}


exports.getblogbycategory= async (req, res, next) => {
//    console.log(req.query.page_no)
    var tag = req.query.tag.replace(/%20/g, " ");
    const paginate = (query, { page, pageSize }) => {
        const offset = page * pageSize;
        const limit = pageSize;
      
        return {
          ...query,
          offset,
          limit,
        };
      };
    blogs.findAll(
        paginate(
            {
                where:{ tag: tag ,verified: true },
            },
            {
                page: req.query.page_no,// your page number
                pageSize:10,// your limit        
            }
   //      order: [["createdAt" ,'DESC']],
    ))
    .then(resData=>{                
        return res.status(200).json({
            resData
        })
    })
    .catch(err=>{
        console.log(err);
        return res.status(500).json({
            err:err
        });
    })

}


exports.createblog = async (req, res, next) => {

    blogs.create({
        title:req.body.title,
        image:req.body.image,
        text:req.body.text,
        createdby:req.body.userid,
        tag:req.body.tag,
        verified:false
    }).then(resData=>{                
        return res.status(200).json({
            message:'Created successfully'
        })
    }).catch(err=>{
        console.log(err);
        return res.status(500).json({
            err:err
        });
    })
}
exports.deleteblog = async (req, res, next) => {
    blogs.destroy({
        where:{ id: req.query.id  }
    })
    .then(resData=>{                
        return res.status(200).json({
            message:'Deleted Successfully'
        })
    })
    .catch(err=>{
        console.log(err);
        return res.status(500).json({
            err:err
        });
    })
}


exports.updateblog = async (req, res, next) => {
   
    console.log(req.body);
    blogs.findByPk(req.body.id)
    .then(blogresult => {


        blogresult.title = req.body.title || blogresult.title;
        blogresult.image = req.body.image || blogresult.image;
        blogresult.text = req.body.text || blogresult.text;
        blogresult.tag = req.body.tag || blogresult.tag;
        blogresult.verified = false;

       
        blogresult.save()
         .then(result => {

           res.status(200).json({
             message: "Operation Sucessful",
             user: result
           

            })    }) 
         
         .catch(err => {
           return res.status(500).json({
             message: "User Update failed",
             error: err.message || "internal server error!"
           });
         });
       });


}

exports.verifyblog = async (req, res, next) => {
   
    blogs.findByPk(req.query.id)
    .then(blogresult => {


        blogresult.verified = true;

       
        blogresult.save()
         .then(result => {

           res.status(200).json({
             message: "Operation Sucessful",
             user: result
           

            })    }) 
         
         .catch(err => {
           return res.status(500).json({
             message: "User Update failed",
             error: err.message || "internal server error!"
           });
         });
       });
}



exports.unverifyblog = async (req, res, next) => {
   
    blogs.findByPk(req.query.id)
    .then(blogresult => {


        blogresult.verified = false;

       
        blogresult.save()
         .then(result => {

           res.status(200).json({
             message: "Operation Sucessful",
             user: result
           

            })    }) 
         
         .catch(err => {
           return res.status(500).json({
             message: "User Update failed",
             error: err.message || "internal server error!"
           });
         });
       });
}



exports.markfeaturedblog = async (req, res, next) => {
   
    blogs.findByPk(req.query.id)
    .then(blogresult => {


        blogresult.featured = true;

       
        blogresult.save()
         .then(result => {

           res.status(200).json({
             message: "Operation Sucessful",
             user: result
           

            })    }) 
         
         .catch(err => {
           return res.status(500).json({
             message: "User Update failed",
             error: err.message || "internal server error!"
           });
         });
       });
}


exports.markunfeaturedblog = async (req, res, next) => {
   
    blogs.findByPk(req.query.id)
    .then(blogresult => {


        blogresult.featured = false;

       
        blogresult.save()
         .then(result => {

           res.status(200).json({
             message: "Operation Sucessful",
             user: result
           

            })    }) 
         
         .catch(err => {
           return res.status(500).json({
             message: "User Update failed",
             error: err.message || "internal server error!"
           });
         });
       });
}


exports.getAdminblogs = async (req, res, next) => {

    blogs.findAll({
        include: [
            {
              association:'User',
              attributes: ['id','email','firstName','lastName']                        
            }
          ],
        order: [["updatedAt" ,'DESC']],
    })
    .then(resData=>{                
        return res.status(200).json({
            blogs:resData
        })
    })
    .catch(err=>{
        console.log(err);
        return res.status(500).json({
            err:err
        });
    })
}

const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
var otpGenerator = require('otp-generator');
dataBase = require('../models');
const User = dataBase.User;
const sequelize = require("sequelize");
var helpers = require("./helpers");
const nodemailer = require("nodemailer");
const { google } = require("googleapis");
const { nanoid } = require('nanoid');

exports.createUser = async (req, res, next) => {
      console.log("\n\nCreate user starts here\n\n");

      //basic requirements check 

      if (!req.body.user) {
        return res.status(400).json({
          message: "user object is not present!"
        });
      }

      if (!req.body.user.password) {
        return res.status(400).json({
          message: "password is not present!"
        });
      }

      if (!req.body.user.email) {
        return res.status(400).json({
          message: "email is not present!"
        });
      }

      if (!req.body.user.firstName) {
        return res.status(400).json({
          message: "first name is not present!"
        });
      } 
      if (!req.body.user.lastName) {
        return res.status(400).json({
          message: "Last Name is not present!"
        });
      }
      if (!req.body.user.role) {
        return res.status(400).json({
          message: "Role is not present!"
        });
      }
      if (!req.body.user.userPrivilegeId) {
        return res.status(400).json({
          message: "userPrivilegeId is not present!"
        });
      }
          //creating company object
              //creating user object
              var otp = nanoid(10);
              bcrypt.hash(req.body.user.password, 10).then(hash => {
                const userObject = {
                  firstName: req.body.user.firstName,
                  lastName: req.body.user.lastName,
                  email: req.body.user.email,
                  encryptedPassword: hash,
                  lastSignIn: null,
                  IP: req.headers['x-forwarded-for'] ||
                    req.socket.remoteAddress ||
                    '',
                  signInCount: null,
                  mobile: req.body.user.mobile || null,
                  role: req.body.user.role,
                  userPrivilegeId: req.body.user.userPrivilegeId,
                //  companyId: companyID,
                  verifed: false,
                  blackList: false,
                  otp:otp

                };
                User.create(userObject)
                  .then(userResponse => {
                  var resp = {
                    id:userResponse.id,
                    otp:otp
                  }
                  var resp1 = JSON.stringify(resp);
                  console.log(resp1);
                    // var transport = nodemailer.createTransport({ // Yes. SMTP!
                    //   host: 'smtp.gmail.com',
                    //   port: 465,
                    //   secure: true,
                    //   auth: {
                    //       user: "bkfl1122@gmail.com", // Use from Amazon Credentials
                    //       pass: "Website112233" // Use from Amazon Credentials
                    //   },
    
                    // });
                    var transport = nodemailer.createTransport({ // Yes. SMTP!
                      name: 'www.learntohack.com.au',
                      host : 'smtp.office365.com',
                      auth: {
                          user: "info@learntohack.com.au", // Use from Amazon Credentials
                          pass: "Sul05846" // Use from Amazon Credentials
                      },
                      secureConnection: false,
                      port: 587,
                      tls: {
                      ciphers:'SSLv3'
                      },
                  });
                        
                        var mailOptions = {
                          from: "'info@learntohack.com.au'<info@learntohack.com.au>", // sender address
                          to: `${req.body.user.firstName} <${req.body.user.email}>`, // list of receivers
                          subject: "Verify User email from learntohack", // Subject line
                          html: `
                          <!DOCTYPE html>
<html>
<head>

  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Email Confirmation</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style type="text/css">
  /**
   * Google webfonts. Recommended to include the .woff version for cross-client compatibility.
   */
  @media screen {
    @font-face {
      font-family: 'Source Sans Pro';
      font-style: normal;
      font-weight: 400;
      src: local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/ODelI1aHBYDBqgeIAH2zlBM0YzuT7MdOe03otPbuUS0.woff) format('woff');
    }
    @font-face {
      font-family: 'Source Sans Pro';
      font-style: normal;
      font-weight: 700;
      src: local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/toadOcfmlt9b38dHJxOBGFkQc6VGVFSmCnC_l7QZG60.woff) format('woff');
    }
  }
  /**
   * Avoid browser level font resizing.
   * 1. Windows Mobile
   * 2. iOS / OSX
   */
  body,
  table,
  td,
  a {
    -ms-text-size-adjust: 100%; /* 1 */
    -webkit-text-size-adjust: 100%; /* 2 */
  }
  /**
   * Remove extra space added to tables and cells in Outlook.
   */
  table,
  td {
    mso-table-rspace: 0pt;
    mso-table-lspace: 0pt;
  }
  /**
   * Better fluid images in Internet Explorer.
   */
  img {
    -ms-interpolation-mode: bicubic;
  }
  /**
   * Remove blue links for iOS devices.
   */
  a[x-apple-data-detectors] {
    font-family: inherit !important;
    font-size: inherit !important;
    font-weight: inherit !important;
    line-height: inherit !important;
    color: inherit !important;
    text-decoration: none !important;
  }
  /**
   * Fix centering issues in Android 4.4.
   */
  div[style*="margin: 16px 0;"] {
    margin: 0 !important;
  }
  body {
    width: 100% !important;
    height: 100% !important;
    padding: 0 !important;
    margin: 0 !important;
  }
  /**
   * Collapse table borders to avoid space between cells.
   */
  table {
    border-collapse: collapse !important;
  }
  a {
    color: #1a82e2;
  }
  img {
    height: auto;
    line-height: 100%;
    text-decoration: none;
    border: 0;
    outline: none;
  }
  </style>

</head>
<body style="background-color: #e9ecef;">

  <!-- start preheader -->
  <div class="preheader" style="display: none; max-width: 0; max-height: 0; overflow: hidden; font-size: 1px; line-height: 1px; color: #fff; opacity: 0;">
        Signup action performed on learntohack.com website. Email is to Verify user email.
  </div>
  <!-- end preheader -->

  <!-- start body -->
  <table border="0" cellpadding="0" cellspacing="0" width="100%">

    <!-- start logo -->
    <tr>
      <td align="center" bgcolor="#e9ecef">
        <!--[if (gte mso 9)|(IE)]>
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
        <tr>
        <td align="center" valign="top" width="600">
        <![endif]-->
        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
          <tr>
            <td align="center" valign="top" style="padding: 36px 24px;">
              <a href="https://learntohack.com.au/" target="_blank" style="display: inline-block;">
                <img src="https://blogwebspace.sgp1.digitaloceanspaces.com/%27info%40learntohack.com.au%27/logo.png" alt="Logo" border="0" width="100" style="display: block; width: 100px; max-width: 100px; min-width: 100px;">
              </a>
            </td>
          </tr>
        </table>
        <!--[if (gte mso 9)|(IE)]>
        </td>
        </tr>
        </table>
        <![endif]-->
      </td>
    </tr>
    <!-- end logo -->

    <!-- start hero -->
    <tr>
      <td align="center" bgcolor="#e9ecef">
        <!--[if (gte mso 9)|(IE)]>
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
        <tr>
        <td align="center" valign="top" width="600">
        <![endif]-->
        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
          <tr>
            <td align="left" bgcolor="#ffffff" style="padding: 36px 24px 0; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; border-top: 3px solid #d4dadf;">
            <h1 style="margin: 0; font-size:16px; font-weight: normal; letter-spacing: -1px; line-height: 48px;">
            We will value your contribution to learntohack.com. You are about to do something good for the Infosec community.
            </h1>
            <p style="margin: 0; font-size: 16px; font-weight: normal; letter-spacing: -1px; line-height: 48px;">Confirm Your Email Address</p>
            <p style="margin: 0; font-size: 25px; font-weight: bold; letter-spacing: -1px; line-height: 48px;"><a href="#">${userResponse.email}</a></p>
              </td>
          </tr>
        </table>
        <!--[if (gte mso 9)|(IE)]>
        </td>
        </tr>
        </table>
        <![endif]-->
      </td>
    </tr>
    <!-- end hero -->

    <!-- start copy block -->
    <tr>
      <td align="center" bgcolor="#e9ecef">
        <!--[if (gte mso 9)|(IE)]>
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
        <tr>
        <td align="center" valign="top" width="600">
        <![endif]-->
        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">

          <!-- start copy -->
          <tr>
            <td align="left" bgcolor="#ffffff" style="padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">
              <p style="margin: 0;">Tap the button below to confirm your email address. If you didn't create an account with <a href="https://learntohack.com.au/">learntohack.com</a>, you can safely delete this email.</p>
            </td>
          </tr>
          <!-- end copy -->

          <!-- start button -->
          <tr>
            <td align="left" bgcolor="#ffffff">
              <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                  <td align="center" bgcolor="#ffffff" style="padding: 12px;">
                    <table border="0" cellpadding="0" cellspacing="0">
                      <tr>
                        <td align="center" bgcolor="#1a82e2" style="border-radius: 6px;">
                          <a href="https://learntohack.com.au/verifyuser/${userResponse.id}/${otp}" target="_blank" style="display: inline-block; 
                          padding: 16px 36px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; 
                          font-size: 16px; color: #ffffff; text-decoration: none; border-radius: 6px;">Activate Account</a>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <!-- end button -->

          <!-- start copy
          <tr>
            <td align="left" bgcolor="#ffffff" style="padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">
              <p style="margin: 0;">If that doesn't work, copy and paste the following link in your browser:</p>
              <p style="margin: 0;"><a href="https://blogdesire.com" target="_blank">https://blogdesire.com/xxx-xxx-xxxx</a></p>
            </td>
          </tr>
           end copy -->

          <!-- start copy -->
          <tr>
            <td align="left" bgcolor="#ffffff" style="padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; border-bottom: 3px solid #d4dadf">
              <p style="margin: 0;">Regards,<br> Team learntohack.com</p>
            </td>
          </tr>
          <!-- end copy -->

        </table>
        <!--[if (gte mso 9)|(IE)]>
        </td>
        </tr>
        </table>
        <![endif]-->
      </td>
    </tr>
    <!-- end copy block -->

    <!-- start footer -->
    <tr>
      <td align="center" bgcolor="#e9ecef" style="padding: 24px;">
        <!--[if (gte mso 9)|(IE)]>
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
        <tr>
        <td align="center" valign="top" width="600">
        <![endif]-->
        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">

          <!-- start permission -->
          <tr>
            <td align="center" bgcolor="#e9ecef" style="padding: 12px 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px; color: #666;">
              <p style="margin: 0;">You received this email because we received a request for Signup for your account. If you didn't request Signup you can safely delete this email.</p>
            </td>
          </tr>
          <!-- end permission -->

          <!-- start unsubscribe
          <tr>
            <td align="center" bgcolor="#e9ecef" style="padding: 12px 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px; color: #666;">
              <p style="margin: 0;">To stop receiving these emails, you can <a href="https://sendgrid.com" target="_blank">unsubscribe</a> at any time.</p>
              <p style="margin: 0;">Paste 1234 S. Broadway St. City, State 12345</p>
            </td>
          </tr>
          end unsubscribe -->

        </table>
        <!--[if (gte mso 9)|(IE)]>
        </td>
        </tr>
        </table>
        <![endif]-->
      </td>
    </tr>
    <!-- end footer -->

  </table>
  <!-- end body -->

</body>
</html>`
                          
                                                };
                        
                        // send mail with defined transport object
                        transport.sendMail(mailOptions, function(error, response){
                          if(error){
                              console.log(error);
                          }else{
                              console.log("Message sent: " + response.message);
                          }
                          transport.close(); // shut down the connection pool, no more messages
                          return res.status(200).json({
                            message: 'Emailed successfully',
                         });              
                        
                        });

                    // return res.status(200).json({
                    //   message: "User Created  Successful.",
                    //   user: userResponse,

                    // })


            }) // to observe response async
            .catch(err => {
              return res.status(400).json({
                message: "Email address or mobile number already exist.!",
                error: err.message || "Internal server Error"
              })
            }); 
  })};

exports.userLogin = (req, res, next) => {
  let fetchedUser;

  if (req.body.email) {
    User.findOne({ where: { email: req.body.email } }
    )
      .then(user => {
        if (!user) {
          return res.status(401).json({
            message: "Auth failed,email not found"
          });
        }
        fetchedUser = user;
        return bcrypt.compare(req.body.password, user.encryptedPassword);
      })
      .then(result => {
        if (!result) {
          return res.status(401).json({
            message: "Auth failed,password mismatched."
          });
        }
        if (fetchedUser.verifed)
        {
        const token = jwt.sign(
          { email: fetchedUser.email, userId: fetchedUser.id },
          "akmakndalknadfa"
        );
           res.status(200).json({
              token: token,
              user: fetchedUser
            });
        }else{
          return res.status(400).json({
            message: "User not verified."
          });
        }


      })
      .catch(err => {
        return res.status(401).json({
          message: "Invalid authentication credentials!"
        });
      });
  }
  else {
    return res.status(400).json({
      message: "email and mobile missing!"
    });
  }
  
};


exports.getUserById = (req, res, next) => {

      if (!req.query.id) {
        return res.status(400).json({
          message: "User Id not present"
        });
      }
      User.findByPk(req.query.id)
        .then(result => {

    
      const user = {
        id: result.id,
        firstName: result.firstName || '',
        lastName: result.lastName || '',
        email: result.email || '',
        mobile: result.mobile,
        role: result.role || '',
      
      }
      
          return res.status(200).json({
                message: "Success!",
                user: user, 
                  })})
               .catch(
                err => {
                  return res.status(500).json({
                    message: "User Privilege Id present but  data fetching failed",
                    error: err.message || "internal server error!"
                  });

                }
              )
        
};

exports.updateUserById = async (req, res, next) => {
      if (!req.body.id) {
        return res.status(400).json({
          message: "User Id not present!"
        })
      }
      if (!req.body) {
        return res.status(400).json({
          message: "user object is not present!"
        });
      }


        User.findByPk(req.body.id)
         .then(userResult => {
     

            userResult.firstName = req.body.firstName || userResult.firstName;
            userResult.lastName = req.body.lastName || userResult.lastName;
            userResult.email = req.body.email || userResult.email;
            userResult.mobile = req.body.mobile || userResult.mobile;
            userResult.role = req.body.role || userResult.role;
            
            userResult.save()
              .then(result => {

                res.status(200).json({
                  message: "Operation Sucessful",
                  user: result
                

                 })    }) 
              
              .catch(err => {
                return res.status(500).json({
                  message: "User Update failed",
                  error: err.message || "internal server error!"
                });
              });
            });

}




exports.verifyuser = async (req, res, next) => {
   
  User.findByPk(req.query.id)
  .then(userresult => {

    if(userresult.otp == req.query.otp)
    {
      userresult.verifed = true;

     
      userresult.save()
     .then(result => {

       res.status(200).json({
         message: "User Verified Successfully."
       

        })    }) 
     
     .catch(err => {
       return res.status(500).json({
         message: "User Update failed",
         error: err.message || "internal server error!"
       });
     });

    }
    else
    {
      return res.status(400).json({
        message: "User Not Verified.",
        error: err.message || "internal server error!"
      }); 
    }
     });


}

exports.getAllUsers = async (req, res, next) => {
   
  User.findAll(
    {
      where:{role:"customer"},
      order: [["updatedAt" ,'DESC']],
    }
  ).then(userresult => {

         res.status(200).json({
           usersList: userresult
       });
     }).catch(err => {
      return res.status(500).json({
        message: "User Update failed",
        error: err.message || "internal server error!"
      });
    });



}

exports.forgetPasswordEmail = async (req, res, next) => {

  if(!req.query.email)
  {
    return res.status(500).json({
      message: 'No email found.',
   });     
  }
  User.findOne({where:{email:req.query.email}}).then(response=>{
        if(response.firstName!='')
        {
          var orderId = nanoid(10);
          bcrypt.hash(orderId, 10).then(hash => {
            response.encryptedPassword=hash;
            response.save().then(result=>{
              var transport = nodemailer.createTransport({ // Yes. SMTP!
                name: 'www.learntohack.com.au',
                host : 'smtp.office365.com',
                auth: {
                    user: "info@learntohack.com.au", // Use from Amazon Credentials
                    pass: "Sul05846" // Use from Amazon Credentials
                },
                secureConnection: false,
                port: 587,
                tls: {
                ciphers:'SSLv3'
                },
            });
                    var mailOptions = {
                      from: "'info@learntohack.com.au'<info@learntohack.com.au>", // sender address
                      to: `${response.firstName} <${response.email}>`, // list of receivers
                      subject: "Forget password email from learntohack", // Subject line
                      html: `
                      <!DOCTYPE html>
                          <html>
                          <head>

                          <meta charset="utf-8">
                          <meta http-equiv="x-ua-compatible" content="ie=edge">
                          <title>Email Confirmation</title>
                          <meta name="viewport" content="width=device-width, initial-scale=1">
                          <style type="text/css">
                          /**
                          * Google webfonts. Recommended to include the .woff version for cross-client compatibility.
                          */
                          @media screen {
                          @font-face {
                            font-family: 'Source Sans Pro';
                            font-style: normal;
                            font-weight: 400;
                            src: local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/ODelI1aHBYDBqgeIAH2zlBM0YzuT7MdOe03otPbuUS0.woff) format('woff');
                          }
                          @font-face {
                            font-family: 'Source Sans Pro';
                            font-style: normal;
                            font-weight: 700;
                            src: local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/toadOcfmlt9b38dHJxOBGFkQc6VGVFSmCnC_l7QZG60.woff) format('woff');
                          }
                          }
                          /**
                          * Avoid browser level font resizing.
                          * 1. Windows Mobile
                          * 2. iOS / OSX
                          */
                          body,
                          table,
                          td,
                          a {
                          -ms-text-size-adjust: 100%; /* 1 */
                          -webkit-text-size-adjust: 100%; /* 2 */
                          }
                          /**
                          * Remove extra space added to tables and cells in Outlook.
                          */
                          table,
                          td {
                          mso-table-rspace: 0pt;
                          mso-table-lspace: 0pt;
                          }
                          /**
                          * Better fluid images in Internet Explorer.
                          */
                          img {
                          -ms-interpolation-mode: bicubic;
                          }
                          /**
                          * Remove blue links for iOS devices.
                          */
                          a[x-apple-data-detectors] {
                          font-family: inherit !important;
                          font-size: inherit !important;
                          font-weight: inherit !important;
                          line-height: inherit !important;
                          color: inherit !important;
                          text-decoration: none !important;
                          }
                          /**
                          * Fix centering issues in Android 4.4.
                          */
                          div[style*="margin: 16px 0;"] {
                          margin: 0 !important;
                          }
                          body {
                          width: 100% !important;
                          height: 100% !important;
                          padding: 0 !important;
                          margin: 0 !important;
                          }
                          /**
                          * Collapse table borders to avoid space between cells.
                          */
                          table {
                          border-collapse: collapse !important;
                          }
                          a {
                          color: #1a82e2;
                          }
                          img {
                          height: auto;
                          line-height: 100%;
                          text-decoration: none;
                          border: 0;
                          outline: none;
                          }
                          </style>

                          </head>
                          <body style="background-color: #e9ecef;">

                          <!-- start preheader -->
                          <div class="preheader" style="display: none; max-width: 0; max-height: 0; overflow: hidden; font-size: 1px; line-height: 1px; color: #fff; opacity: 0;">
                                Forget Password action performed on learntohack.com website. New password has been emailed to you. 
                          </div>
                          <!-- end preheader -->

                          <!-- start body -->
                          <table border="0" cellpadding="0" cellspacing="0" width="100%">

                          <!-- start logo -->
                          <tr>
                            <td align="center" bgcolor="#e9ecef">
                              <!--[if (gte mso 9)|(IE)]>
                              <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
                              <tr>
                              <td align="center" valign="top" width="600">
                              <![endif]-->
                              <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                                <tr>
                                  <td align="center" valign="top" style="padding: 36px 24px;">
                                    <a href="http://blogfrontend.s3-website-us-east-1.amazonaws.com/" target="_blank" style="display: inline-block;">
                                    <img src="https://blogwebspace.sgp1.digitaloceanspaces.com/%27info%40learntohack.com.au%27/logo.png" alt="Logo" border="0" width="100" style="display: block; width: 100px; max-width: 100px; min-width: 100px;">
                                    </a>
                                  </td>
                                </tr>
                              </table>
                              <!--[if (gte mso 9)|(IE)]>
                              </td>
                              </tr>
                              </table>
                              <![endif]-->
                            </td>
                          </tr>
                          <!-- end logo -->

                          <!-- start hero -->
                          <tr>
                            <td align="center" bgcolor="#e9ecef">
                              <!--[if (gte mso 9)|(IE)]>
                              <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
                              <tr>
                              <td align="center" valign="top" width="600">
                              <![endif]-->
                              <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                                <tr>
                                  <td align="left" bgcolor="#ffffff" style="padding: 36px 24px 0; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; border-top: 3px solid #d4dadf;">
                                  <h1 style="margin: 0; font-size: 16px; font-weight: normal; letter-spacing: -1px; line-height: 48px;">We're glad you're here</h1>
                                  <p style="margin: 0; font-size: 16px; font-weight: normal; letter-spacing: -1px; line-height: 48px;">Forget Password Email</p>
                                  <p style="margin: 0; font-size: 16px; font-weight: normal; letter-spacing: -1px; line-height: 48px;"><a href="">${response.email}</a></p>
                                  </td>
                                </tr>
                              </table>
                              <!--[if (gte mso 9)|(IE)]>
                              </td>
                              </tr>
                              </table>
                              <![endif]-->
                            </td>
                          </tr>
                          <!-- end hero -->

                          <!-- start copy block -->
                          <tr>
                            <td align="center" bgcolor="#e9ecef">
                              <!--[if (gte mso 9)|(IE)]>
                              <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
                              <tr>
                              <td align="center" valign="top" width="600">
                              <![endif]-->
                              <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">


                                <!-- start button -->
                                <tr>
                                  <td align="left" bgcolor="#ffffff">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                      <tr>
                                        <td align="center" bgcolor="#ffffff" style="padding: 12px;">
                                          <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                              <td align="center" style="border-radius: 6px;font-size:22px;">
                                                New Password: ${orderId}
                                              </td>
                                            </tr>
                                          </table>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                                <!-- end button -->

                                <!-- start copy
                                <tr>
                                  <td align="left" bgcolor="#ffffff" style="padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">
                                    <p style="margin: 0;">If that doesn't work, copy and paste the following link in your browser:</p>
                                    <p style="margin: 0;"><a href="https://blogdesire.com" target="_blank">https://blogdesire.com/xxx-xxx-xxxx</a></p>
                                  </td>
                                </tr>
                                end copy -->

                                <!-- start copy -->
                                <tr>
                                  <td align="left" bgcolor="#ffffff" style="padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; border-bottom: 3px solid #d4dadf">
                                    <p style="margin: 0;">Regards,<br> Team learntohack.com</p>
                                  </td>
                                </tr>
                                <!-- end copy -->

                              </table>
                              <!--[if (gte mso 9)|(IE)]>
                              </td>
                              </tr>
                              </table>
                              <![endif]-->
                            </td>
                          </tr>
                          <!-- end copy block -->

                          <!-- start footer -->
                          <tr>
                            <td align="center" bgcolor="#e9ecef" style="padding: 24px;">
                              <!--[if (gte mso 9)|(IE)]>
                              <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
                              <tr>
                              <td align="center" valign="top" width="600">
                              <![endif]-->
                              <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">

                                <!-- start permission -->
                                <tr>
                                  <td align="center" bgcolor="#e9ecef" style="padding: 12px 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px; color: #666;">
                                    <p style="margin: 0;">You received this email because we received a request for Forget Password for your account. If you didn't request for new password you can safely delete this email.</p>
                                  </td>
                                </tr>
                                <!-- end permission -->

                                <!-- start unsubscribe
                                <tr>
                                  <td align="center" bgcolor="#e9ecef" style="padding: 12px 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px; color: #666;">
                                    <p style="margin: 0;">To stop receiving these emails, you can <a href="https://sendgrid.com" target="_blank">unsubscribe</a> at any time.</p>
                                    <p style="margin: 0;">Paste 1234 S. Broadway St. City, State 12345</p>
                                  </td>
                                </tr>
                                end unsubscribe -->

                              </table>
                              <!--[if (gte mso 9)|(IE)]>
                              </td>
                              </tr>
                              </table>
                              <![endif]-->
                            </td>
                          </tr>
                          <!-- end footer -->

                          </table>
                          <!-- end body -->

                          </body>
                          </html>` // email body
                    };
                    
                    // send mail with defined transport object
                    transport.sendMail(mailOptions, function(error, response){
                      if(error){
                          console.log(error);
                      }else{
                          console.log("Message sent: " + response.message);
                      }
                      transport.close(); // shut down the connection pool, no more messages
                      return res.status(200).json({
                        message: 'Emailed successfully',
                     });              
                    
                    });
              })
          });
        }
        else{
          return res.status(400).json({
            message: 'No user found with this email.',
         });        
        }
  }).catch(err=>{
    return res.status(500).json({
      message: 'No user found with this email.',
   });     
  })
  
}

exports.resetPassword = async (req, res, next) => {

  User.findByPk(req.body.user.id).then(response=>{
    if(response)
    {
      bcrypt.compare(req.body.user.oldPassword, response.encryptedPassword).then(result=>{
        if(!result)
        {
          return res.status(500).json({
            message: 'Old Password Mismatch.',
          });
        }
        else{
          bcrypt.hash(req.body.user.newPassword, 10).then(hash => {
            response.encryptedPassword=hash;
            response.save().then(result=>{
              return res.status(200).json({
                message: 'Password updated sucessfully.',
              });
            });
          });
        }
    }).catch(err=>{
      return res.status(500).json({
        message: 'Old Password Mismatch.',
      });
    })

    }        
  })
}
dataBase = require('../models');
const Privilege = dataBase.UserPrivilege;
exports.getAllPrivileges = (req, res, next) => {
    
  Privilege.findAll()
   .then(result => {
       return res.status(200).json({
           message: "success",
           user_privileges: result
       })
   }).catch(err=>{
       return res.status(500).json({
            error: err.message || "internal server error" 
       });
   }) 
   
  
};

exports.getBasicCustomerPrivilege = (req, res, next) =>{
    Privilege.findOne({where:{allowed_type: 'customer', description: 'basic user'}})
    .then(result => {
        return res.status(200).json({
            message: "success",
            basic: result
        })
    }).catch(err=>{
        return res.status(500).json({
             error: err.message || "internal server error" 
        });
    }) 
    
}

exports.getAdminTypePrivileges = (req, res, next) => {
    
    Privilege.findAll({where:{allowed_type: 'admin'}})
     .then(result => {
         return res.status(200).json({
             message: "success",
             user_privileges: result
         })
     }).catch(err=>{
         return res.status(500).json({
              error: err.message || "internal server error" 
         });
     }) 
     
    
  };




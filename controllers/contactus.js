dataBase = require('../models');
const contactus = dataBase.contactus;
const sequelize = require("sequelize");
const nodemailer = require("nodemailer");
exports.createContactUs = async (req, res, next) => {

    contactus.create({
        name:req.body.name,
        company_name:req.body.company_name,
        email:req.body.email,
        reason:req.body.reason,
        contact_number:req.body.mobile,
        message:req.body.message,
    }).then(resData=>{                
        var transport = nodemailer.createTransport({ // Yes. SMTP!
            name: 'www.learntohack.com.au',
            host : 'smtp.office365.com',
          //  port: 465,
        //    secure: true,
            auth: {
                user: "info@learntohack.com.au", // Use from Amazon Credentials
                pass: "Sul05846" // Use from Amazon Credentials
            },
            secureConnection: false,
            port: 587,
            tls: {
            ciphers:'SSLv3'
            },
        });
  //   var transport = nodemailer.createTransport({ // Yes. SMTP!
  //       service:'gmail',
  //       host: 'smtp.gmail.com',
  //       port: 465,
  //       secure: true,
  //       auth: {
  //           user: "bkfl1122@gmail.com", // Use from Amazon Credentials
  //           pass: "roaiyleygibfbtuq" // Use from Amazon Credentials
  //       },
  // });

              var mailOptions = {
                from: "'info@learntohack.com'<info@learntohack.com.au>", // sender address
                to: `info@learntohack.com`, // list of receivers
                subject: `Contact Us Request from ${req.body.email}`, // Subject line
                html: `
                <!DOCTYPE html>
<html>
<head>

<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Email Confirmation</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style type="text/css">
/**
* Google webfonts. Recommended to include the .woff version for cross-client compatibility.
*/
@media screen {
@font-face {
font-family: 'Source Sans Pro';
font-style: normal;
font-weight: 400;
src: local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/ODelI1aHBYDBqgeIAH2zlBM0YzuT7MdOe03otPbuUS0.woff) format('woff');
}
@font-face {
font-family: 'Source Sans Pro';
font-style: normal;
font-weight: 700;
src: local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/toadOcfmlt9b38dHJxOBGFkQc6VGVFSmCnC_l7QZG60.woff) format('woff');
}
}
/**
* Avoid browser level font resizing.
* 1. Windows Mobile
* 2. iOS / OSX
*/
body,
table,
td,
a {
-ms-text-size-adjust: 100%; /* 1 */
-webkit-text-size-adjust: 100%; /* 2 */
}
/**
* Remove extra space added to tables and cells in Outlook.
*/
table,
td {
mso-table-rspace: 0pt;
mso-table-lspace: 0pt;
}
/**
* Better fluid images in Internet Explorer.
*/
img {
-ms-interpolation-mode: bicubic;
}
/**
* Remove blue links for iOS devices.
*/
a[x-apple-data-detectors] {
font-family: inherit !important;
font-size: inherit !important;
font-weight: inherit !important;
line-height: inherit !important;
color: inherit !important;
text-decoration: none !important;
}
/**
* Fix centering issues in Android 4.4.
*/
div[style*="margin: 16px 0;"] {
margin: 0 !important;
}
body {
width: 100% !important;
height: 100% !important;
padding: 0 !important;
margin: 0 !important;
}
/**
* Collapse table borders to avoid space between cells.
*/
table {
border-collapse: collapse !important;
}
a {
color: #1a82e2;
}
img {
height: auto;
line-height: 100%;
text-decoration: none;
border: 0;
outline: none;
}
</style>

</head>
<body style="background-color: #e9ecef;">

<!-- start preheader -->
<div class="preheader" style="display: none; max-width: 0; max-height: 0; overflow: hidden; font-size: 1px; line-height: 1px; color: #fff; opacity: 0;">
Contact Us action performed on learntohack.com website.
</div>
<!-- end preheader -->

<!-- start body -->
<table border="0" cellpadding="0" cellspacing="0" width="100%">

<!-- start logo -->
<tr>
<td align="center" bgcolor="#e9ecef">
<!--[if (gte mso 9)|(IE)]>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
<tr>
<td align="center" valign="top" width="600">
<![endif]-->
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
<tr>
  <td align="center" valign="top" style="padding: 36px 24px;">
    <a href="https://learntohack.com.au/" target="_blank" style="display: inline-block;">
      <img src="https://blogwebspace.sgp1.digitaloceanspaces.com/%27info%40learntohack.com.au%27/logo.png" alt="Logo" border="0" width="100" style="display: block; width: 100px; max-width: 100px; min-width: 100px;">
    </a>
  </td>
</tr>
</table>
<!--[if (gte mso 9)|(IE)]>
</td>
</tr>
</table>
<![endif]-->
</td>
</tr>
<!-- end logo -->

<!-- start hero -->
<tr>
<td align="center" bgcolor="#e9ecef">
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
<tr>
  <td align="left" bgcolor="#ffffff" style="padding: 36px 24px 0; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; border-top: 3px solid #d4dadf;">
  <h1 style="margin: 0; font-size:16px; font-weight: normal; letter-spacing: -1px; line-height: 48px;">
  We will value your contribution to learntohack.com. You are about to do something good for the Infosec community.
  </h1>
  <p style="margin: 0; font-size: 16px; font-weight: normal; letter-spacing: -1px; line-height: 48px;">Contact Us Request From</p>
  <p style="margin: 0; font-size: 25px; font-weight: bold; letter-spacing: -1px; line-height: 48px;"><a href="#">${req.body.email}</a></p>
    </td>
</tr>
</table>
<!--[if (gte mso 9)|(IE)]>
</td>
</tr>
</table>
<![endif]-->
</td>
</tr>
<!-- end hero -->

<!-- start copy block -->
<tr>
<td align="center" bgcolor="#e9ecef">
<!--[if (gte mso 9)|(IE)]>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
<tr>
<td align="center" valign="top" width="600">
<![endif]-->
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">

<!-- start copy -->
<tr>
  <td align="left" bgcolor="#ffffff" style="padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">
    <p style="margin: 0;">You have received one new contact us request.</p>
  </td>
</tr>
<!-- end copy -->

<!-- start button -->
<tr>
  <td align="left" bgcolor="#ffffff">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td align="center" bgcolor="#ffffff" style="padding: 12px;">
          <table border="0" cellpadding="0" cellspacing="0">
            <tr>
            <td>
                <h1 style="font-size:16px;">Name :</h1><br>
                <p>${req.body.name}</p>
             </td>
            </tr>
            <br>
            <tr>
            <td>
                <h1 style="font-size:16px;">Contact Number:</h1>  <br>
                <p>${req.body.mobile}</p>
             </td>
            </tr>
            <br>
            <tr>
            <td>
                <h1 style="font-size:16px;">Reason:</h1>          <br>
                <p>${req.body.reason}</p>
             </td>
            </tr>
            <br>
            <tr>
            <td>
                <h1 style="font-size:16px;">Message:</h1>           <br> 
                <p>${req.body.message}</p>
             </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </td>
</tr>
<!-- end button -->


<!-- start copy -->
<tr>
  <td align="left" bgcolor="#ffffff" style="padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; border-bottom: 3px solid #d4dadf">
    <p style="margin: 0;">Regards,<br> Team learntohack.com</p>
  </td>
</tr>
<!-- end copy -->

</table>
<!--[if (gte mso 9)|(IE)]>
</td>
</tr>
</table>
<![endif]-->
</td>
</tr>
<!-- end copy block -->

<!-- start footer -->
<tr>
<td align="center" bgcolor="#e9ecef" style="padding: 24px;">
<!--[if (gte mso 9)|(IE)]>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
<tr>
<td align="center" valign="top" width="600">
<![endif]-->
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">

<!-- start permission -->
<tr>
  <td align="center" bgcolor="#e9ecef" style="padding: 12px 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px; color: #666;">
    <p style="margin: 0;">You received this email because we received a request for Contact Us Request for your account.</p>
  </td>
</tr>
<!-- end permission -->


</table>
<!--[if (gte mso 9)|(IE)]>
</td>
</tr>
</table>
<![endif]-->
</td>
</tr>
<!-- end footer -->

</table>
<!-- end body -->

</body>
</html>`
                
                                      };
              
              // send mail with defined transport object
              transport.sendMail(mailOptions, function(error, response){
                if(error){
                    console.log(error);
                }else{
                    console.log("Message sent: " + response.message);
                }
                transport.close(); // shut down the connection pool, no more messages
                return res.status(200).json({
                  message: 'Emailed successfully',
               });              
              
              });
}).catch(err=>{
        console.log(err);
        return res.status(500).json({
            err:err
        });
    })
}
exports.getAllContactUs = async (req, res, next) => {

    contactus.findAll({
        order: [["updatedAt" ,'DESC']],
    })
    .then(resData=>{                
        return res.status(200).json({
            contactUs:resData
        })
    })
    .catch(err=>{
        console.log(err);
        return res.status(500).json({
            err:err
        });
    })
}
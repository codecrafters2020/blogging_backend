dataBase = require('../models');
const forum_comments = dataBase.forum_comments;
//1 for newspress
//2 for memberpress


exports.createforumcomments = async (req, res, next) => {

    forum_comments.create({
    
        comment:req.body.comment,
        createdby:req.body.userid,
        forumId:req.body.forumId,
    }).then(resData=>{                
        return res.status(200).json({
            message:'Created successfully'
        })
    }).catch(err=>{
        console.log(err);
        return res.status(500).json({
            err:err
        });
    })
}
exports.deleteforumcomments  = async (req, res, next) => {
    forum_comments.destroy({
        where:{ id: req.query.id  }
    })
    .then(resData=>{                
        return res.status(200).json({
            message:'Deleted Successfully'
        })
    })
    .catch(err=>{
        console.log(err);
        return res.status(500).json({
            err:err
        });
    })
}




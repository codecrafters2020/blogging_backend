dataBase = require('../models');
const forums = dataBase.forum;
const sequelize = require("sequelize");

//1 for newspress
//2 for memberpress

exports.getAllforums= async (req, res, next) => {

    forums.findAll({
        where:{ verified: true},
        offset: req.query.page_no,// your page number
        limit:10,// your limit
        include: [
            { 
              association:'forum_comments',
            }
          ],
       
            order: [["updatedAt" ,'DESC']],
          
         
    })
    .then(resData=>{                
        return res.status(200).json({
            blogs:resData
        })
    })
    .catch(err=>{
        console.log(err);
        return res.status(500).json({
            err:err
        });
    })
}
exports.getforumsbyid = async (req, res, next) => {

    forums.findOne({
        where:{ id: req.query.id  },
        include: [
          { 

          
            association: 'forum_comments',
            as: 'comments',
            include: [
                { association:'User',
                   
                    attributes: ['id','email','firstName','lastName']
                }
              ]
        
             
          }
        ]
    })
    .then(resData=>{                
        return res.status(200).json({
            resData
        })
    })
    .catch(err=>{
        console.log(err);
        return res.status(500).json({
            err:err
        });
    })
}




exports.gettags = async (req, res, next) => {

    forums.findAll({
        where:{ verified: true},

        attributes: [           
            [sequelize.fn('DISTINCT', sequelize.col('tag')) ,'tag'],
        ]
    })
    .then(resData=>{                
        return res.status(200).json({
            tag:resData
        })
    })
    .catch(err=>{
        console.log(err);
        return res.status(500).json({
            err:err
        });
    })
}

exports.getforumbycategory= async (req, res, next) => {

    forums.findAll({
        where:{ tag: req.body.tag ,verified: true },
        include: [
          { association:'forum_comments',
             
          }
        ]
    })
    .then(resData=>{                
        return res.status(200).json({
            resData
        })
    })
    .catch(err=>{
        console.log(err);
        return res.status(500).json({
            err:err
        });
    })
}


exports.createforums = async (req, res, next) => {

    forums.create({
        title:req.body.title,
        image:req.body.image,
        text:req.body.text,
        createdby:req.body.userid,
        verified:false,
        tag:req.body.tag
    }).then(resData=>{                
        return res.status(200).json({
            message:'Created successfully'
        })
    }).catch(err=>{
        console.log(err);
        return res.status(500).json({
            err:err
        });
    })
}
exports.deleteforums = async (req, res, next) => {
    forums.destroy({
        where:{ id: req.query.id  }
    })
    .then(resData=>{                
        return res.status(200).json({
            message:'Deleted Successfully'
        })
    })
    .catch(err=>{
        console.log(err);
        return res.status(500).json({
            err:err
        });
    })
}
exports.verifyforum = async (req, res, next) => {
   
    forums.findByPk(req.query.id)
    .then(forumsresult => {


        forumsresult.verified = true;

       
        forumsresult.save()
         .then(result => {

           res.status(200).json({
             message: "Operation Sucessful",
             user: result
           

            })    }) 
         
         .catch(err => {
           return res.status(500).json({
             message: "User Update failed",
             error: err.message || "internal server error!"
           });
         });
       });
}


exports.unverifyforum = async (req, res, next) => {
   
    forums.findByPk(req.query.id)
    .then(forumsresult => {


        forumsresult.verified = false;

       
        forumsresult.save()
         .then(result => {

           res.status(200).json({
             message: "Operation Sucessful",
             user: result
           

            })    }) 
         
         .catch(err => {
           return res.status(500).json({
             message: "User Update failed",
             error: err.message || "internal server error!"
           });
         });
       });
}



exports.markfeaturedforum = async (req, res, next) => {
   
    forums.findByPk(req.query.id)
    .then(forumsresult => {


        forumsresult.featured = true;

       
        forumsresult.save()
         .then(result => {

           res.status(200).json({
             message: "Operation Sucessful",
             user: result
           

            })    }) 
         
         .catch(err => {
           return res.status(500).json({
             message: "User Update failed",
             error: err.message || "internal server error!"
           });
         });
       });
}


exports.markunfeaturedforum = async (req, res, next) => {
   
    forums.findByPk(req.query.id)
    .then(forumsresult => {


        forumsresult.featured = false;

       
        forumsresult.save()
         .then(result => {

           res.status(200).json({
             message: "Operation Sucessful",
             user: result
           

            })    }) 
         
         .catch(err => {
           return res.status(500).json({
             message: "User Update failed",
             error: err.message || "internal server error!"
           });
         });
       });
}




exports.getadminforums= async (req, res, next) => {

    forums.findAll({

        order: [["updatedAt" ,'DESC']],
        include: [
            { 
  
            
              association: 'forum_comments',
              association:'User',
              attributes: ['id','email','firstName','lastName']          
               
            }
          ]
    })
    .then(resData=>{                
        return res.status(200).json({
            blogs:resData
        })
    })
    .catch(err=>{
        console.log(err);
        return res.status(500).json({
            err:err
        });
    })
}

exports.getAllMyforums = async (req, res, next) => {

    forums.findAll({
        where:{ createdby:req.query.id },

        order: [["updatedAt" ,'DESC']],
    })
    .then(resData=>{                
        return res.status(200).json({
            blogs:resData
        })
    })
    .catch(err=>{
        console.log(err);
        return res.status(500).json({
            err:err
        });
    })
}


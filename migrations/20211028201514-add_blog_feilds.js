'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
     queryInterface.addColumn('blogs', 'read_count',{
      type: Sequelize.DataTypes.INTEGER      
    })

    queryInterface.addColumn('users', 'createdAt',{
      type: Sequelize.DataTypes.DATE,
      defaultValue: null,
      allowNull: true
      
    }),
    queryInterface.addColumn('users', 'updatedAt',{
      type: Sequelize.DataTypes.DATE,
      defaultValue: null,
      allowNull: true
      
    })


  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};

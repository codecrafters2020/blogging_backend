'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {

    return Promise.all([
      queryInterface.changeColumn('blogs', 'text', {
        type: Sequelize.TEXT,
        allowNull: true
    }), 
    queryInterface.changeColumn('forums', 'text', {
      type: Sequelize.TEXT,
      allowNull: true
  }, 
      ),
    ])

  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};

'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      firstName: {
        type: Sequelize.STRING
      },
      lastName: {
        type: Sequelize.STRING
      },
      email: {
        type: Sequelize.STRING,
        unique: true
      },
     
      userPrivilegeId: {
        type: Sequelize.DataTypes.INTEGER,
        references: {
          model: {
            tableName: 'user_privileges',
            schema: 'public'
          },
          key: 'id'
        },
        allowNull: false
      },
    
     encryptedPassword:{
      allowNull: false,
      type: Sequelize.STRING
    },
    lastSignIn:{
      type: Sequelize.STRING
    },
    IP:{
      allowNull: false,
      type: Sequelize.STRING
    },
    signInCount:{
      type: Sequelize.INTEGER
    },
    mobile:{
     allowNull: false,
      type: Sequelize.STRING,
      unique: true
    },
    role: {
      allowNull: false,
      type: Sequelize.ENUM,
      values: ['admin', 'customer'],
      
    },

    verifed:{
      type: Sequelize.BOOLEAN
    },
    blackList: {
      type: Sequelize.BOOLEAN
    },
   



    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('users');
  }
};